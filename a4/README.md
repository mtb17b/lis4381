> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 Mobile Web Application Development

## Marcus Brown

### LIS 4381 Requirements:

*Sub-Heading:*

1. Create a Online Portfolio 
2. Complete skillset screenshots 10-12
3. Chapter Questions (Chapters 9, 10, 15)

#### README.md file should include the following items:

* Screenshots of Online Portfolio
* Screenshot of skillset 10 - Array List
* Screenshot of skillset 11 - Alpha Numeric Special
* Screenshot of skillset 12 - Temperature Conversion

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
#### Assignment Screenshots:

| *Screenshot of Passed Validation*             | *Screenshot of Failed Validation*
| -----------                                   | -----------
| ![Passed Validation](img3/Screen_One.png)     | ![Failed Validation](img3/Screen_Two.png)

*Screenshot of Online Portfolio Homepage*:

![Online Portfolio](img3/Screen_Three.png "Online Portfolio")

*Screenshot of Skillset 10 Array List*:

![Array List Screenshot](img3/Array_List.png "Array List")

*Screenshot of Skillset 11 Alpha Numeric Special*:

![Alpha Numeric Special Screenshot](img3/Alpha_Numeric_Special.png "Alpha Numeric Special")

*Screenshot of Skillset 12 Temperature Conversion*:

![Temperature Conversion Screenshot](img3/Temperature_Conversion.png "Temperature Conversion")

*Online Portfolio*:
[My Online Portfolio](http://localhost:8080/repos/lis4381/index.php "My Online Portfolio")
