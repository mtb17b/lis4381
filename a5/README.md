> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 Mobile Web Application Development

## Marcus Brown

### LIS 4381 Requirements:

*Sub-Heading:*

1. Test Validations JQuery
2. Create A5 using Bootstrap w/ PHP
3. Complete skillset screenshots 13-15
4. Chapter Questions (Chapters 11, 12, 19)

#### README.md file should include the following items:

* Screenshots of Failed and Passed Validation
* Screenshot of skillset 13 - Sphere Volume Calculator
* Screenshot of skillset 14 - PHP: Simple Calculator
* Screenshot of skillset 15 - PHP: Write/Read File

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
#### Assignment Screenshots:

*Screenshot of Index Table*

![Index Table](images/Table_1.png "Index Table")

| *Screenshot of Invalid Server-side Validation*| *Screenshot of Invalid Validation*                    |
| -----------                                   | -----------                                           |
| ![Failed Validation](images/Form_Wrong.png)   | ![Failed Validation](images/Failed_Connection.png)    |


| *Screenshot of Valid Server-side Validation*  | *Screenshot of Valid Server-side Validation*          |
| -----------                                   | -----------                                           |
| ![Valid Validation](images/Form_Right.png)    | ![Valid Validation](images/Table_2.png)               |

*Screenshot of Skillset 13*

![Sphere Volume Calculator](images/Sphere_Volume_Calculator.png "Sphere Volume Calculator")

*Screenshot of Skillset 14*

| *Screenshot of Simple Calculator*                      | *Screenshot of Simple Calculator*                        |
| -----------                                            | -----------                                              |
| ![Simple Calculator](images/Simple_Calculator_1.png)   | ![Simple Calculator](images/Simple_Calculator_2.png)     |

| *Screenshot of Simple Calculator*                      | *Screenshot of Simple Calculator*                        |
| -----------                                            | -----------                                              |
| ![Simple Calculator](images/Simple_Calculator_3.png)   | ![Simple Calculator](images/Simple_Calculator_4.png)     |

*Screenshot of Skillset 15*

| *Screenshot of Write/Read File*                      | *Screenshot of Write/Read File*                            |
| -----------                                          | -----------                                                |
| ![Simple Calculator](images/write_read_one.png)      | ![Simple Calculator](images/write_read_two.png)            |
