> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS 4381 Mobile Web Application Developments

## Marcus Brown

### LIS 4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Install JDK
    - Install Android Studio
    - Provide screenshots of installations

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create a Recipe app using Android Studio
    - Complete skillset 1-3
    - Provide screenshots for completion

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create a Concert app using Android Studio
    - Complete skillset screenshots 4-6
    - Complete database model for Pet's R-Us)

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Clone Repository
    - Create a Online Portfolio 
    - Complete skillset screenshots 10-12
    - Provide screenshots for completion

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Test Validations JQuery
    - Create A5 using Bootstrap w/ PHP
    - Complete skillset screenshots 13-15
    - Chapter Questions (Chapters 11, 12, 19)
    
6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create a launcher icon image and display it in both activities (screens) 
    - Must add background color(s) to both activities 
    - Must add border around image and button 
    - Must add text shadow (button) 

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Add Edit/Update Functionality
    - Delete Functionality
    - RSS Feed

    
    