> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 Mobile Web Application Development

## Marcus Brown

### LIS 4381 Requirements:

*Sub-Heading:*

1. RSS Feed Addition
2. Bootstrap Validations
3. Chapter Questions (Chapters 13, 14)

#### README.md file should include the following items:

* Screenshots of RSS Feed
* Screenshot of Delete Record attempt
* Screenshot of Passed and Failed Validations
* Link to Localhost

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
#### Assignment Screenshots:


| *Screenshot of Portfolio Homepage*            | *Screenshot of Table*                    |
| -----------                                   | -----------                              |
| ![Portfolio Homepage](img/homepage.png)       | ![Index Table](img/index.png)            |


| *Screenshot of Edit Petstore Form*            | *Screenshot of Failed Validation*                  |
| -----------                                   | -----------                                        |
| ![Edit Petstore](img/edit_petstore.png)       | ![Failed Validation](img/failed_validation.png)    |


| *Screenshot of Passed Validation*                     | *Screenshot of Delete Record Prompt*              |
| -----------                                           | -----------                                       |
| ![Passed Validation](img/passed_validation.png)       | ![Delete Record Prompt](img/delete_record.png)    |


| *Screenshot of Sucessfully Deleted Record*            | *Screenshot of RSS Feed*                 |
| -----------                                           | -----------                              |
| ![Sucessfully Deleted Record](img/success_delete.png) | ![RSS Feed](img/rss_feed.png)            |