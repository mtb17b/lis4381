> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 Mobile Web Application Development

## Marcus Brown

### LIS 4381 Requirements:

*Sub-Heading:*

1. Create a Concert app using Android Studio
2. Complete skillset screenshots 4-6
3. Complete database model for Pet's R-Us)

#### README.md file should include the following items:

* Screenshot of Screen 1 and 2 of Concert App
* Screenshot of skillset 4 - Decision Structure
* Screenshot of skillset 5 - Random Number Generator
* Screenshot of skillset 6 - Methods

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
#### Assignment Screenshots:

*Screenshot of Concert Screen 1*:

![Concert Screen 1 Screenshot](img2/Concert_Screen_One.png "Concert Screen One")

*Screenshot of Concert Screen 2*:

![Concert Recipe Screen 2 Screenshot](img2/Concert_Screen_Two.png "Concert Screen Two")

*Screenshot of ERD*:
![ERD Screenshot](img2/E_R_D.png "ERD")

*Screenshot of ERD1*:
![ERD 1 Screenshot](img2/one.png "ERD 1")

*Screenshot of ERD2*:
![ERD 2 Screenshot](img2/two.png "ERD 2")

*Screenshot of ERD3*:
![ERD 3 Screenshot](img2/three.png "ERD 3")

*Screenshot of Skillset 4 Decision Structure*:

![Decision Structure Screenshot](img2/Decision_Structure.png "Decision Structure")

*Screenshot of Skillset 5 Random Number Generator*:

![Random Number Generator Screenshot](img2/Random_Number_Generator.png "Random Number Generator")

*Screenshot of Skillset 6 Methods*:

![Methods Screenshot](img2/Methods.png "Methods")
