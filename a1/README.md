> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 Mobile Web Application Development

## Marcus Brown

### LIS 4381 Requirements:

*Sub-Heading:*

1. Distributed Version Control wit Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chapters 1,2)

#### README.md file should include the following items:

* Screenshot of AMPPS installation.
* Screenshot of running java Hello
* Screenshot of Android Studio - My First App
* git command and descriptions

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - creates a new GIT repository. Can be used to convert a current repository or a empty new one.
2. git status - shows a list of the files that were changed or need to be changed. Literally shows the status of files.
3. git add - adds one or many files to the next stage of being pused through. Collecting all updates mades to go to the final stage which is commiting.
4. git commit - finalizing all the changes that has been made to the repository. What is known as a final draft.
5. git push - sends the changes to the master branch of the repository. Updating with new content.
6. git pull - fetching content from the remote repository and updating the local repository with it. Making sure the local and remote repository look the same.
7. (additional command) git config - used to set configurations specific to the user. This can inlcude usernames, emails, and so on.

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ammps3.png "My AMPPS file")

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png "My JDK file")

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mtb17b/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/mtb17b/myteamquotes/ "My Team Quotes Tutorial")