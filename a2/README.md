> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 Mobile Web Application Development

## Marcus Brown

### LIS 4381 Requirements:

*Sub-Heading:*

1. Created a Recipe app using Android Studio
2. Completed skillset screenshots 1-3
3. Chapter Questions (Chapters 3, 4)

#### README.md file should include the following items:

* Screenshot of Screen 1 and 2 of Recipe App
* Screenshot of skillset 1 - Even or Odd
* Screenshot of skillset 2 - Largest Number
* Screenshot of skillset 3 - Loops and Arrays

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
#### Assignment Screenshots:

*Screenshot of Bruschetta Recipe Screen 1*:

![Bruschetta Recipe Screen 1 Screenshot](images/Bruschetta_Recipe_Screen_1.png "Bruschetta Recipe Screen One")

*Screenshot of Bruschetta Recipe Screen 2*:

![Bruschetta Recipe Screen 2 Screenshot](images/Bruschetta_Recipe_Screen_2.png "Bruschetta Recipe Screen Two")

*Screenshot of Skillset 1 Even or Odd*:

![Even Or Odd Screenshot](images/Even_Or_Odd.png "Even or Odd")

*Screenshot of Skillset 2 Largest Number*:

![Largest Number Screenshot](images/Largest_Number.png "Largest Number")

*Screenshot of Skillset 3 Loops and Arrays*:

![Loops And Arrays Screenshot](images/Loops_And_Arrays.png "Loops and Arrays")
