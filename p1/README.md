> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 Mobile Web Application Development

## Marcus Brown

### LIS 4381 Requirements:

*Sub-Heading:*

1. Create a launcher icon image and display it in both activities (screens) 
2. Must add background color(s) to both activities 
3. Must add border around image and button 
4. Must add text shadow (button) 

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1; 
* Screenshot of running application’s first user interface; 
* Screenshot of running application’s second user interface;
* Screenshot of skillset 7 - Random NUmber Generator Data Validation
* Screenshot of skillset 8 - Largest Three Numbers
* Screenshot of skillset 9 - Array Runtime Data Validation   

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
#### Assignment Screenshots:

*Screenshot of Business Card Screen 1*:

![Business Card Screen 1 Screenshot](images2/Business_Card_Screen_6.png "Business Card Screen One")

*Screenshot of Business Screen 2*:

![Business Card Recipe Screen 2 Screenshot](images2/Business_Card_Screen_5.png "Business Card Screen Two")

*Screenshot of Skillset 7 Random Number Generator Data Validation*:

![Random Number Generator Data Validation Screenshot 1](images2/Random_Number_Generator_Data_Validation.png "Random Number Generator Data Validation 1")

![Random Number Generator Data Validation Screenshot 2](images2/Random_Number_Generator_Data_Validation_2.png "Random Number Generator Data Validation 2")

![Random Number Generator Data Validation Screenshot 3](images2/Random_Number_Generator_Data_Validation_3.png "Random Number Generator Data Validation 3")

*Screenshot of Skillset 8 Largest Three Numbers*:

![Largest Three Numbers Screenshot](images2/Largest_Three_Numbers.png "Largest Three Numbers")

*Screenshot of Skillset 9 Array Runtime Data Validation*:

![Array Runtime Data Validation Screenshot](images2/Array_Runtime_Data_Validation.png "Array Runtime Data Validation")
