import java.util.Scanner;

public class Main
{
    public static void main(String args[])
    {
        //call static void methods (i.e, no obkect, non-value returning)
        Methods.getRequirements();
        Methods.getUserPhoneType();
    }
}